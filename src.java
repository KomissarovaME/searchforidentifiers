package org.komissarova.search_for_identifiers;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Search {

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\b[a-zA-Z0-9_]\\w+\\b");
        Matcher matcher = pattern.matcher("");
        
        try(FileWriter fw = new FileWriter("ids.txt")) {
            List<String> lines = Files.readAllLines(Paths.get("src.java"));
            for(String line : lines) {
                matcher.reset(line);
                while(matcher.find()) {
                    fw.write(matcher.group());
                    fw.write("\n");
                }
            }
        } catch(IOException ioe) {
            System.out.print("Что-то пошло не так: ");
            System.out.println(ioe);
        }
    }

}
